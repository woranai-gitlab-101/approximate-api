package main

import (
	"app/database"
	"app/util"
	"log"
	"net/http"

	"github.com/gorilla/mux"
	solr "github.com/rtt/Go-Solr"
)

var (
	sessionDB *solr.Connection
)

func main() {
	//จัดการ route ด้วย gorilla mux
	r := mux.NewRouter()
	r.HandleFunc("/", index).Methods("GET")
	r.HandleFunc("/data/{core}/query", querySelect).Methods("GET")

	//connect ตัว solr db โดยป้อน input core ชื่อว่า "test_set"
	connect, err := database.ConnectSolr("test_set")
	if err != nil {
		log.Println(err)
	}
	sessionDB = connect

	//รับ api ขึ้นมาตาม port docker
	log.Println("ListenAndServe api started :8080")
	http.ListenAndServe(":8080", r)
}

func index(w http.ResponseWriter, r *http.Request) {
	util.ResponseOk(w, 200, "[√] Service approximate search started.")
}

type Data struct {
	Core   string `json:"core"`
	Result struct {
		Name  string  `json:"name"`
		Score float64 `json:"score"`
	}
}

func querySelect(w http.ResponseWriter, r *http.Request) {
	//รับค่าจาก route
	vars := mux.Vars(r)
	core := vars["core"]

	//กำหนดค่าให้ response
	data := &Data{}
	data.Core = core
	data.Result.Name = "ทดสอบ"
	data.Result.Score = 6.88

	//response ข้อมูลออกเป็น json
	util.ResponseJSON(w, http.StatusOK, map[string]interface{}{
		"data":    data,
		"message": "data retrievedsuccess",
		"status":  "OK",
	})
}
