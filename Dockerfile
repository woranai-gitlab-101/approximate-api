# Build stage
FROM golang:1.13.5-alpine AS builder

ENV GO111MODULE=on
WORKDIR /app
COPY . .

# Install dependencies
RUN go get -d -v ./...
RUN go install -v ./...

# Testing and build binary file
# RUN CGO_ENABLED=0 go test -cover ./...
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -a -ldflags "-s -w" -o main

# Final stage
FROM alpine:3.6
RUN apk add --no-cache ca-certificates openssl tzdata
RUN cp /usr/share/zoneinfo/Asia/Bangkok /etc/localtime \
    && echo "Asia/Bangkok" > /etc/timezone \
    && echo "Asia/Bangkok" > /etc/TZ \
    && unset TZ
COPY --from=builder \
    /app/main ./
CMD ["./main"]