package database

import (
	"log"

	solr "github.com/rtt/Go-Solr"
)

type database struct {
	solr *solr.Connection
}

func ConnectSolr(core string) (*solr.Connection, error) {
	s, err := solr.Init("localhost", 8983, core)
	if err != nil {
		log.Fatalf("[x] Solr connection error: %s", err)
	}
	log.Println("[√] Connected solr successfully")
	session := s
	return session, err
}
